FROM alpine:latest

MAINTAINER Leo Cheron <leo@cheron.works>

# ssh
RUN apk --no-cache add bash git openssh rsync && \
    mkdir -p ~root/.ssh && chmod 700 ~root/.ssh/ && \
    echo -e "Port 22\n" >> /etc/ssh/sshd_config

# build dependencies
RUN apk --no-cache add python build-base

# nodejs
RUN apk --no-cache add nodejs && \
	npm install --global gulp-cli
